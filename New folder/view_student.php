	<HTML>
	<HEAD><TITLE>Viewing Student Data</TITLE><HEAD>
	<BODY>
	
	<h1>View Student Data</h1>
	
		<?php
	     require ("config.php"); //read up on php includes https://www.w3schools.com/php/php_includes.asp

	     $sql = "SELECT * FROM Student";
		 $result = mysqli_query($conn, $sql);
	
		 if (mysqli_num_rows($result) > 0) { 	?>
			 
		<!-- Start table tag -->
		<table width="600" border="1" cellspacing="0" cellpadding="3">
		 
		<!-- Print table heading -->
		<tr>
		<td align="center"><strong>Name</strong></td>
		<td align="center"><strong>IC</strong></td>
		<td align="center"><strong>Matric</strong></td>
		<td align="center"><strong>Update</strong></td>
		<td align="center"><strong>Delete</strong></td>
		</tr> 
		
		<?php
			// output data of each row
			while($rows = mysqli_fetch_assoc($result)) {
		?>
		
	     <tr>
			<td><?php echo $rows['name']; ?></td>
			<td><?php echo $rows['ic']; ?></td>
			<td><?php echo $rows['matric']; ?></td>
			<td align="center"> <a href="update_form.php?id=<?php echo $rows['id']; ?>">update</a> </td>
			<td align="center"> <a href="delete_student.php?id=<?php echo $rows['id']; ?>">delete</a> </td>
		</tr>

		<?php
			}
		} else {
			echo "0 results";
			}
	     
	     mysqli_close($conn);
	   ?>
	    
	    </table>
	
		<BR><BR>
		<a href="student_form.php">Click here to insert student</a>
		<BR><BR>
	    <a href="index.php">Back to Main Page</a>
 
	</BODY>
	</HTML>
