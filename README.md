Git global setup
git config --global user.name "Firdaus Azman"
git config --global user.email "muhdfirdausn@gmail.com"

Push an existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/ad-group-7/clinical-and-pharmacist-system-2.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/ad-group-7/clinical-and-pharmacist-system-2.git
git push -u origin --all
git push -u origin --tags
