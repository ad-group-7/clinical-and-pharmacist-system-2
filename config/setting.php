<?php

$servername = "localhost";
$username = "root";
$password = "";
$database = "clinic_system";

// Create connection
$virtual_con = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$virtual_con) {
  die("Connection failed: " . mysqli_connect_error());
}
?>
